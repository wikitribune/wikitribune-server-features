Feature: Writing articles
  As a reporter
  In order to share important facts
  I want to publish an article about them

Feature: Submitting drafts
  As a reporter
  In order to document important facts
  I want to draft information about them


Scenario: Writing a new article
  Given I am the registered user named "Jimmy Wales"
   Then there should be zero article drafts in the database
    And I should have no articles published in my name
   When I submit a new article draft titled "Making WikiTRIBUNE" with the following content:
    """
    Hello world!  I'm a guy with a keyboard.  Let's make a journal together!
    """
    And I print the response
   Then there should be one article draft in the database
    And there should be an article draft titled "Making WikiTRIBUNE"
    And I still should have no articles published in my name
   When I publish that article
    And I print the response
   Then there should be zero article drafts in the database
    And there should be a published article titled "Making WikiTRIBUNE"
    And I now should have one article published in my name


Scenario: Failing to write a new article when not logged in
  Given I am not logged in
   Then there should be no article drafts in the database
   When I try to submit a new article draft titled "We are Anonymous" without content
    And I print the response
   Then my request should be denied
   Then there should still be no article drafts in the database


# Ideas for debate, to poll
# Note that we may move tolerance to (Multimedia) Drafts,
# conceptually, and reserve Article for published things.
#Rule: Articles SHOULD have a title
#Rule: Articles MUST have a title upon publication
#Rule: Articles MUST each have a unique title
#Rule: Articles' titles SHOULD not change after publication
#Rule: Articles' titles  MUST  not change after publication

Scenario Outline: Failing to write a new article with a title too short
  Given I am the registered user named "Uninspired Reporter"
   Then there should be no article drafts in the database
   When I try to submit a new article draft titled "<title>" with no content
   #And I print the response
   Then my request should be denied
    And there should be no article drafts in the database
  Examples:
  | title |
  | a     |
  | ∞     |
  | abc   |
  | -o-   |
  | 💎💎💎 |


@wip @tbd @help
Scenario: Succeeding in writing an article with a title of 4 characters


Scenario: Editing my own article draft
  Given I am the registered user named "Tintin"
    And I submit a new article draft titled "Rêveries from Congo" with the following content:
    """
    Arriving soon in Congo! ETA 1 day.
    """
   Then a year passes
   When I edit the article draft titled "Rêveries from Congo" with the following content:
    """
    Arriving soon in Congo! ETA 1 day.
    …
    What happened there? Milou?
    What have I done? What are we doing?
    """
   Then there should be an article draft titled "Rêveries from Congo" containing:
    """
    Arriving soon in Congo! ETA 1 day.
    …
    What happened there? Milou?
    What have I done? What are we doing?
    """


Scenario: Failing to edit an article when not logged in
  Given I am not logged in
    And there already is an article draft titled "Draft" in the database
    And there already is a published article titled "Article" in the database
   When I try to edit the article draft titled "Draft" with the following content:
    """
    Ipsum
    """
   Then my request should be denied
   When I try to edit the published article titled "Article" with the following content:
    """
    Ipsum
    """
   Then my request should be denied


Scenario: Editing someone else's article draft
  Given I am the registered user named "リュウ"
    And I submit a new article draft titled "Encounter" with the following content:
    """
    リュウ: はどうけん?
    """
   Then my request should be accepted
  Given I am now the registered user named "ケン"
   When I edit the article draft titled "Encounter" with the following content:
    """
    リュウ: はどうけん?
    ケン: 来いよ！　熱くさせてやるぜ！
    """
   Then my request should be accepted
  Given I am now the registered user named "リュウ"
   When I edit the article draft titled "Encounter" with the following content:
    """
    リュウ: はどうけん?
    ケン: 来いよ！　熱くさせてやるぜ！
    リュウ: 昇龍拳!
    """
   Then my request should be accepted
    And there should be an article draft titled "Encounter" containing:
    """
    リュウ: はどうけん?
    ケン: 来いよ！　熱くさせてやるぜ！
    リュウ: 昇龍拳!
    """
