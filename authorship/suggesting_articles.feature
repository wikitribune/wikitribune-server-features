Feature: Suggesting articles
  As an interested party
  In order to signal my interest about a subject
  I want to suggest an article about it


Scenario: Suggesting a new article
  Given I am the registered user named "RoiPoussière"
   Then there should be zero article suggestions in the database
   When I suggest a new article titled "Zeste de Savoir"
   Then there should be an article suggestion in the database
   Then there should still be no article drafts in the database
   Then there should still be no article publications in the database


Scenario: Failing to suggest a new article with an already taken headline
  Given I am the registered user named "RoiPoussière"
   Then there should be zero article suggestions in the database
   When I suggest a new article titled "Zeste de Savoir"
   Then there should be an article suggestion in the database
   When I try to suggest again a new article titled "Zeste de Savoir"
   Then I should fail
   Then there should still be one article suggestion in the database
   Then there should still be no article drafts in the database
   Then there should still be no article publications in the database

