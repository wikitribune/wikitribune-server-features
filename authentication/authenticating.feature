Feature: Authenticating
  In order to be able to contribute
  As a person
  I want to authenticate to my account


Scenario: Authenticating to an user account
  Given I register as the new user "François Ruffin" with password "Merci"
   When I print the response
   Then there should be one user in the database
   When I try to authenticate as "François Ruffin" with password "Merci"
   Then I should succeed
    And I print the response


Scenario: Failing to authenticate with the wrong credentials
  Given I register as the new user "François Ruffin" with password "Merci"
   When I try to authenticate as "François Ruffin" with password "Canard"
   Then I should fail


@vigil
Scenario: Failing to authenticate with an empty password
  Given I register as the new user "François Ruffin" with password "Merci"
   When I try to authenticate as "François Ruffin" with password ""
   Then I should fail
    And I print the response

