Feature: Classifying articles
  As an author or an archivist
  In order to help others find relevant articles
  I want to classify articles by topic


# Scenario: Adding a topic to an article
Scenario: Adding an article to a topic
  Given there already is a topic named "Écologie" in the database
    And there already is a topic named "🧀 Cheese" in the database
    And there already is a published article titled "Sècheresse"
    And the article "Sècheresse" should not be in the topic "🧀 Cheese"
    And I am the registered user named "Adèle"
   When I add the article "Sècheresse" to the topic "🧀 Cheese"
   #And I print the last transaction
   Then the article "Sècheresse" should be in the topic "🧀 Cheese"
    But the article "Sècheresse" should not be in the topic "Écologie"
   When I add the article "Sècheresse" to the topic "Écologie"
   Then the article "Sècheresse" should be in the topic "🧀 Cheese"
    And the article "Sècheresse" should also be in the topic "Écologie"


Scenario: Failing to add an article to a topic when not logged in
  Given I am not logged in
    And there already is a topic named "Terrorism" in the database
    And there already is a published article titled "Gilets Jaunes"
   When I try to add the article "Gilets Jaunes" to the topic "Terrorism"
   Then I should fail
   Then the article "Gilets Jaunes" should still not be in the topic "Terrorism"


@vigil
Scenario: Failing to add an article to a topic it is already in
  Given I am the registered user named "Mediacrat"
    And there already is a topic named "Terrorism" in the database
    And there already is a published article titled "Gilets Jaunes"
   When I add the article "Gilets Jaunes" to the topic "Terrorism"
   Then the article "Gilets Jaunes" should be in the topic "Terrorism"
   When I try to add the article "Gilets Jaunes" to the topic "Terrorism"
    And I print the transaction
   Then I should fail
   Then the article "Gilets Jaunes" should still be in the topic "Terrorism"


Scenario: Removing an article from a topic
  Given there already is a topic named "Écologie" in the database
    And there already is a topic named "🧀 Cheese" in the database
    And there already is a published article titled "Sècheresse"
    And the article "Sècheresse" should not be in the topic "🧀 Cheese"
    And I am the registered user named "Adèle"
   When I add the article "Sècheresse" to the topic "🧀 Cheese"
   Then the article "Sècheresse" should be in the topic "🧀 Cheese"
    But the article "Sècheresse" should not be in the topic "Écologie"
   When I remove the article "Sècheresse" from the topic "🧀 Cheese"
    And I print the last transaction
   Then the article "Sècheresse" should not be in the topic "🧀 Cheese"
    And the article "Sècheresse" should also not be in the topic "Écologie"


Scenario: Failing to remove an article from a topic when not logged in
  Given there already is a topic named "Fake" in the database
    And there already is an article like this in the database:
    """
    slug: tronald-dump-2108
    headline: Tronald Dump meets with Selenites for Solar Peace Summit
    topics:
      - Fake
    """
   When I try to remove the article "tronald-dump-2108" from the topic "Fake"
   Then I should fail
   Then the article "tronald-dump-2108" should still be in the topic "Fake"


Scenario: Failing to remove an article from a topic it is not in
  Given I am the registered user named "Adèle"
    And there already is a topic named "🧀 Cheese" in the database
    And there already is a published article titled "Sècheresse"
    And the article "Sècheresse" should not be in the topic "🧀 Cheese"
 #Since the article "Sècheresse" should not be in the topic "🧀 Cheese"
   When I try to remove the article "Sècheresse" from the topic "🧀 Cheese"
   Then I should fail
   Then the article "Sècheresse" should still not be in the topic "🧀 Cheese"

