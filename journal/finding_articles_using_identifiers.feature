Feature: Finding articles using their identifiers
  As a curious reader
  In order to inform myself on a specific subject
  I want to read specific articles


Background:
  Given there already is a published article titled "Article I" in the database
    And there already is a published article titled "Article II" in the database


Scenario: Finding an article by its identifier
   When I request the article with id "1"
   Then I should succeed
    And I print the response
    And that article's title should be "Article I"


Scenario: Finding an article by its slug
   When I request the article with id "article-ii"
   Then I should succeed
    And I print the response
    And that article's title should be "Article II"
    And I print the last transaction


#Scenario: Finding an article by its universally unique identifier
#See https://framagit.org/wikitribune/wikitribune-server-rails/issues/7


Scenario: Failing to find a non-existent article
   When I try to request the article with id "42"
   Then I should fail with code 404
   When I try to request the article with id "the-answer-to-everything"
   Then I should fail
    And I print the response
    And the response's code should be 404
