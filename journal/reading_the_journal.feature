Feature: Reading the journal
  As a random passerby
  In order to inform myself
  I want to read the journal


Background: Lorem Ipsum
  Given there already is a published article titled "Article I" in the database
    And there already is a published article titled "Article II" in the database
    And there already is a published article titled "Article III" in the database
    And there already is a published article titled "Article IV" in the database
    And there already is a published article titled "Article V" in the database


Scenario: Reading the most recently published articles
   When I request the most recently published articles
   Then I should be given five articles
    And these articles should be sorted by publication date, most recent first


@vigil
Scenario: Reading the two most recently published articles
   When I request the two most recently published articles
   Then I should succeed
    And I print the response
    And the response should contain two articles
    And I should be given two articles
    And the articles should be sorted by publication date, most recent first
