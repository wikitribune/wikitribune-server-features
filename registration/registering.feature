Feature: Registering
  In order to be able to author content
  As an influencer|reporter|witness|censor|expert|captain
  I want to register an account on wikitribune


Scenario: Registering a new user account
  Given there should be no users in the database
   When I register as the new user "Rich Holman" with password "In4rchHolm="
   Then there should be one user in the database


Scenario: Failing to register with an existing user name
   When I register as the new user "Rich Holman" with password "In4rchHolm="
   When I try to register as the new user "Rich Holman" with password "In4rchHolm="
   Then I should fail
   Then there should be one user in the database


Scenario: Failing to register with a user name too close to an existing user name
   When I register as the new user "Rich Holman" with password "In4rchHolm="
   When I try to register as the new user "Rïch Hôlmân" with password "In4rchHolm="
   Then I should fail
   Then there should be one user in the database

