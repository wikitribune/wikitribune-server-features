Feature: Curating articles
  As a curator
  In order to maintain the quality of articles through time
  I want to edit articles after their publication


Scenario: Curating someone else's published article
  Given the year is 1891
    And I am the registered user named "Oscar Wilde"
    And I submit a new article draft titled "Soul" with the following content:
    """
    In old days men had the rack.
    Now they have the Press.
    We are dominated by Journalism.
    """
    And I publish that article
   Then Time, the healer, passes for a hundred and twenty seven years
    And I am now the registered user named "Autocorrect"
   When I edit the published article titled "Soul" with the following content:
    """
    In old days humans had the rack.
    Now they have the Press.
    We are dominated by Journalism.
    """
   Then I should succeed (f)
    And there should be a published article titled "Soul" containing:
    """
    In old days humans had the rack.
    Now they have the Press.
    We are dominated by Journalism.
    """
    And that article's diff with its previous version should be:
    """
    -In old days men had the rack.
    +In old days humans had the rack.
    """
