Feature: Initially filling the database
  In order to create an instance of wikitribune with the initial content of my choice
  As a reporter with super cow powers
  I want to fill the database with fixtures from various origins


Scenario: Getting the bare bones
  Given a new instance of wikitribune without fixtures
   Then there should be no users in the database
   Then there should be no article drafts in the database
    And there should be no published articles in the database


#Scenario: Getting lorem ipsum
#Scenario: Getting data from WordPress
#Scenario: Getting data from another live wikitribune instance
#Scenario: Getting data from crawling ActivityPub federated instances
