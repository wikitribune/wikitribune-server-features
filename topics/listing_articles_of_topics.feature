Feature: Listing articles of topics
  In order to find articles relevant to my interests
  As a person
  I want to list the articles of specific topics


Scenario: Reading the most recent articles of a topic
  Given there already is a topic named "World" in the database
    And there already is a topic named "Fact-Checking" in the database
    And there already is an article like this in the database:
    """
    slug: a
    headline: "To be: that sleep; no traveller in thers that is heir"
    article_body: "To die: that that pith a bare bodkin?"
    topics:
      - World
      - Fact-Checking
    """
    And there already is an article like this in the database:
    """
    slug: b
    headline: "Celestially goodness hesitantly meant busted"
    article_body: "A save and camel in more knowingly thus."
    topics:
      - World
    """
   When I request the articles of the topic "World"
    And I print the response
   Then the response should contain two articles
   Then I should be given two articles
   When I request the articles of the topic "Fact-Checking"
   Then the response should contain one article
   Then I should be given one article

