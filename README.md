## What is this?

This is the repository for the server-side features of Wikitribune.

These non-official, work-in-progress features are written in Gherkin.

They look like this:

```gherkin
Feature: …
In order to …
As a …
I want to …

Scenario: …
  Given …
    And …
   When …
   Then …
    But …
```

_(the above is actually a good boilerplate for new features)_


## Summary

An overview of the features is available in [SUMMARY.md](SUMMARY.md).


## What can I do?

Write a new Scenario, either in an existing Feature or a new one.

Curate existing Scenarios.

Up until `v1`, anything can change. Be **bold**.


## Guidelines

_These are not rules, just guidelines._

- Focus a feature on customer needs.
- Limit one feature per feature file.
  This makes it easy to find features.
- Limit the number of scenarios per feature.
  Nobody wants a thousand-line feature file.
  A good measure is a dozen scenarios per feature.
- Limit the number of steps per scenario to less than ten.
  It's okay to go over when initially writing features.
- Limit the character length of each step. Common limits are 80-120 characters.
- Use proper spelling.
- Use proper grammar.
- Capitalize Gherkin keywords.
- Capitalize the first word in titles.
- Do not capitalize words in the step phrases unless they are proper nouns.
- Do not use punctuation (specifically periods `.` and commas `,`) at the end
  of step phrases.
- Use single spaces between words. Never use tabs. _(sorry)_
- Indent the content beneath every section header.
  We've started using a non-conventional indentation for steps,
  which we feel improves readability.
- Separate features and scenarios by two blank lines.
- Separate examples tables by 1 blank line.
- Do not separate steps within a scenario by blank lines.
- Space table delimiter pipes `|` evenly.


### Tags

- Adopt a standard set of tag names. Avoid duplicates.
  - `wip` ignored by CI.
  - `new` freshly-brewed feature, useful only during development
- Write all tag names in lowercase, and use hyphens `-` to separate words.
- Limit the length of tag names.


## More (and sources)

- [gherkin tutorial](http://docs.behat.org/en/v2.5/guides/1.gherkin.html)
- [business-minded overview](https://medium.com/@SteelKiwiDev/how-to-describe-user-stories-using-gherkin-language-8cffc6b888df)
- [guidelines](https://automationpanda.com/2017/01/30/bdd-101-writing-good-gherkin/)
- [advanced tips](https://github.com/cucumber/cucumber/wiki/Cucumber-Backgrounder)